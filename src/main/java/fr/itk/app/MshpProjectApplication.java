package fr.itk.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MshpProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MshpProjectApplication.class, args);
	}

}
