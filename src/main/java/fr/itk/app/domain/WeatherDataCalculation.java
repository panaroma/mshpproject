package fr.itk.app.domain;

import lombok.Data;

@Data
public class WeatherDataCalculation {
	
	private Long noOfStation;
	
	private Long noOfSensorError;
	
	private Double avgValueOfH;
	
	private Double avgValueOfP;
	
	private Double avgValueOfT;
	
	private Double maxValueOfH;
	
	private Double maxValueOfP;
	
	private Double maxValueOfT;
	
	private Double minValueOfH;
	
	private Double minValueOfP;
	
	private Double minValueOfT;
	
	

}
