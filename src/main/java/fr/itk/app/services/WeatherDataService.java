package fr.itk.app.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import fr.itk.app.domain.WeatherDataCalculation;

@Component
public class WeatherDataService implements WeatherDataInterface {

	/* (non-Javadoc)
	 * @see fr.itk.app.services.WeatherDataInterface#getCalculatedData()
	 */
	@Override
	public WeatherDataCalculation getCalculatedData() throws IOException
	{

		WeatherDataCalculation calculatedValues = new WeatherDataCalculation();
		List<String> weatherData = new ArrayList<>();

		try (BufferedReader br = Files.newBufferedReader(Paths.get("src\\main\\resources\\mshp.txt"))) {

						weatherData = br.lines().collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}

		Double averageT = weatherData.stream().sorted().filter(x -> x.startsWith("T,")).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).average().getAsDouble();
		Double averageP = weatherData.stream().sorted().filter(x -> x.startsWith("P,")).filter(x -> isDateValid(x)).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).average().getAsDouble();
		Double averageH = weatherData.stream().sorted().filter(x -> x.startsWith("H,")).mapToDouble(x -> Double.parseDouble(x.replaceAll("\\D+", ""))).average().getAsDouble();

		Double maxValueT = weatherData.stream().sorted().filter(x -> x.startsWith("T,")).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).max().getAsDouble();
		Double maxValueP = weatherData.stream().sorted().filter(x -> x.startsWith("P,")).filter(x -> isDateValid(x)).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).max().getAsDouble();
		Double maxValueH = weatherData.stream().sorted().filter(x -> x.startsWith("H,")).mapToDouble(x -> Double.parseDouble(x.replaceAll("\\D+", ""))).max().getAsDouble();

		Double minValueT = weatherData.stream().sorted().filter(x -> x.startsWith("T,")).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).min().getAsDouble();
		Double minValueP = weatherData.stream().sorted().filter(x -> x.startsWith("P,")).filter(x -> isDateValid(x)).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).min().getAsDouble();
		Double minValueH = weatherData.stream().sorted().filter(x -> x.startsWith("H,")).mapToDouble(x -> Double.parseDouble(x.replaceAll("\\D+", ""))).min().getAsDouble();

		Long noOfWeatherStation = weatherData.stream().sorted().filter(x -> (!x.startsWith("P,"))).filter(x -> (!x.startsWith("T,"))).filter(x -> (!x.startsWith("H,"))).filter(x -> (!x.startsWith("#"))).count();
		Long sensorInerrorCount = weatherData.stream().sorted().filter(x -> x.startsWith("P,")).filter(x -> !isDateValid(x)).mapToDouble(x -> Double.parseDouble(x.substring(x.lastIndexOf(",") + 1, x.length()))).count();

		calculatedValues.setNoOfStation(noOfWeatherStation);
		calculatedValues.setNoOfSensorError(sensorInerrorCount);
		calculatedValues.setMinValueOfT(minValueT);
		calculatedValues.setMinValueOfP(minValueP);
		calculatedValues.setMinValueOfH(minValueH);

		calculatedValues.setMaxValueOfT(maxValueT);
		calculatedValues.setMaxValueOfP(maxValueP);
		calculatedValues.setMaxValueOfH(maxValueH);

		calculatedValues.setAvgValueOfT(averageT);
		calculatedValues.setAvgValueOfP(averageP);
		calculatedValues.setAvgValueOfH(averageH);

		return calculatedValues;

	}

	public static boolean isDateValid(String str) {
		Pattern pattern = Pattern.compile(",(\\d{4}-[01]\\d-[0-3]\\d),");
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
			return true;
		}
		return false;
	}

}
