package fr.itk.app.services;

import java.io.IOException;

import fr.itk.app.domain.WeatherDataCalculation;

public interface WeatherDataInterface {

	WeatherDataCalculation getCalculatedData() throws IOException;

}