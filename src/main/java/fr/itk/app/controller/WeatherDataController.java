package fr.itk.app.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.itk.app.domain.WeatherDataCalculation;
import fr.itk.app.services.WeatherDataInterface;

@RestController
@RequestMapping("/datapoint")
public class WeatherDataController {

	@Autowired
	private WeatherDataInterface wds;

	@RequestMapping(method = RequestMethod.GET, value = "/calcdata")
	@ResponseBody
	public WeatherDataCalculation getData() throws IOException {
		return wds.getCalculatedData();

	}

}
